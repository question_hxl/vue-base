import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router'
import routerCfg from './router'

/* eslint-disable no-new */

Vue.use(VueRouter)

var app = Vue.extend({})

const router = new VueRouter({
  hashbang: true,
  history: true,
  saveScrollPosition: true,
  suppressTransitionError: true
})

routerCfg(router)

router.start(App, '#app')

