/**
 * Created by demo on 2016/4/25.
 */
// var AuthService = require('./services/authenticateService');
import AuthService from './services/authenticateService'

export default function (router) {
  router.map({
    '*': {
      component (resolve) {
        require(['./main'], resolve)
      }
    },
    '/': {
      component (resolve) {
        require(['./main', './views/order'], resolve)
      },
      subRoutes: {
        '/order': {
          component (resolve) {
            require(['./views/order'], resolve)
          }
        },
        '/record': {
          component (resolve) {
            require(['./views/record'], resolve)
          }
        },
        '/register': {
          component (resolve) {
            require(['./views/register'], resolve)
          }
        }
      }
    },
    '/login': {
      component (resolve){
        require(['./views/login'], resolve)
      }
    }
    }
  );

  router.beforeEach(function({to, next}){
    if(!AuthService.isAuthenticated()) {
      router.go('/login');
      next();
    }else {
      next();
    }
  });
}
